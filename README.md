# Math Advance Contributor Guidelines

Are you a new or prospective contributor to Math Advance? Then this is the right place for you! Enclosed here are some contributor tips and guidelines.

Depending on how many permissions you have within Math Advance, you may not be able to see some of the repositories I am mentioning. If you can't see a repository you are interested in, email me at [dchen@mathadvance.org](mailto:dchen@mathadvance.org) and request for access.

## Committing and Pushing

If you have direct push access, **push to dev branches**. Don't push to the main branch for anything other than "version releases" (unless you're in charge of a project, you shouldn't be making version releases). If you don't, fork and pull request once you're done. You should pull request to main since it's actually stable.

# Dev Environments + Building

Some instructions on installing the necessary dev-tools + expectations for commits.

## Linux

There are some scripts like [mast-content](https://gitlab.com/mathadvance/mast/mast-content) that are meant for my use only (this is typically just to build PDFs with LaTeX since installing LaTeX AND compiling serverside would be a big waste of space) or server-side use only. These scripts are usually in Bash and are only written to work with a limited set of environments.

As such if something doesn't work on your Windows machine AND it's not a tool meant for users, chances are it's not supposed to. If something doesn't work on your Windows machine and is MEANT for users, then I have made a mistake, submit an issue or send in a merge request.

## Rust

We have several projects that use Rust and require you to [install it](rust-lang.org/tools/install):
- The [mapm](https://gitlab.com/mathadvance/mapm) family of tools (for contest compilation, think [MAT](mat.mathadvance.org))
- The [mast-build](https://gitlab.com/mathadvance/mast/mast-build) utility for MAST units.

## LaTeX

Most LaTeX projects require certain custom packages or classes. A quick summary of repositories you may need to reference:
- [mast.cls](https://gitlab.com/mathadvance/tex/mast): Formerly known as `lucky.cls`, I've officially moved this over to the MAST repo because 1) I don't use the class for much other than MAST and 2) I don't use any of my other personal classes, like, ever.
- [bounce](https://gitlab.com/dennistex/bounce): Contains `bounce.cls` (no surprise). I don't think mathadvance projects ever use it, but I'm listing it just in case.
- [macontest](https://gitlab.com/mathadvance/tex/macontest): The Math Advance contest class. It's internal and pretty much only used for MAT.
- [mabook](https://gitlab.com/mathadvance/tex/mabook): The Math Advance book class. It's internal because I don't want other people compiling books with our look. But if you intend on writing a book for Math Advance, pitch the idea to me and I'll give you access to the class.
    - [scrambledenvs](https://gitlab.com/chennis/scrambledenvs): Creates scrambled hints and solutions for books. Typically used in *my* books, but other authors may choose to do otherwise.

        Note that the install instructions are different. They are listed in the `scrambledenvs` repository.

Here are the classes you will need to install for the following mathadvance projects:

- `mast`: get `lucky.cls` from `dennistex/personal-tex`. You should clone the entire repository, since `lucky.cls` does not work as a standalone file. 
- `mac-contests`: get `macontest.cls`
- `geo-foray-textbook`: get `mabook.cls` and `scrambledenvs.sty`

### Install Instructions

You should learn about the TEXMF trees if you don't know about them already. It is typically recommended to install custom classes and packages in the `TEXMFHOME` directory, which is located at `~/texmf`.

Go to the directory `~/texmf/tex/latex` (create it if it does not already exist) and clone the GitHub repositories in that directory. That's it.

### Standards

If you have access to the MAST repository, please make sure you compile PDFs correctly. Typically this requires a working `latexmk` setup with a `.latexmkrc` that compiles Asymptote. We no longer ask for or want PDF builds (this is a change from previous guidelines!) but **if you send in a merge request that doesn't correctly compile I will reject it.**

As a rule of thumb, I do not want edits from people using Overleaf or people improperly using Git. This is because these traits are correlated with writing incredibly ugly LaTeX that I have to clean up later (or straight up incorrect LaTeX), and it's just not worth the effort for me. In theory, if you wrote really clean LaTeX and good commit messages, I wouldn't be able to tell the difference, but in practice it's night and day.

If I reject changes it's because they're bad, but the root cause is almost always using technology incorrectly. So I strongly recommend you take the time to get a working LaTeX and Git installation. It literally involves typing
    
    sudo apt install git
    sudo apt install texlive

(or the equivalent) so it really is not that hard to get started.

## Web

There are three main ways you can contribute to the website:

1. Content
2. Visuals (frontend)
3. Website Logic (backend, mostly, but also some Next JS rendering shenanigans)

To propose a change, fork the repository, commit your changes to the fork, and send in a pull request to the master/main branch.

Don't worry about messing things up if you're a beginner. You literally do not have the ability to mess things up besides your own fork because you don't have write access to any of the repositories. Experiment, see what you can learn, and send in a pull request once you have something you're happy with.

### Content

`mast-web` pages are written in MarkdownX. It's like Markdown, but you can also import JavaScript components. In practice you will mostly be writing Markdown and not a lot of X.

Markdown is super easy to learn by example, so I will not elaborate there. Just look at the source of this file!

### Visuals

We use Tailwind CSS in pretty much all of our projects.

### Logic

We use Next JS for all of our projects and slug through `pages` for `.mdx` (MarkdownX) files. As for backend, `mast-web` uses MongoDB and Redis with Next JS APIs. We are using Vultr to host our servers.
